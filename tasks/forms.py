from django.forms import ModelForm
from tasks.models import Task
from django import forms

class CreateTaskForm(ModelForm):
    class Meta:
        model = Task
        widgets = {
            'start_date': forms.DateInput(attrs={'placeholder': 'MM-DD-YYYY'}),
            'due_date': forms.DateInput(attrs={'placeholder': 'MM-DD-YYYY'}),
        }
        fields = [
            "name",
            "start_date",
            "due_date",
            "project",
            "assignee",
        ]
